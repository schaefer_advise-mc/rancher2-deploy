## Purpose:

Re-deploy / deploy new GitLab Registry-Images to Rancher 2.x Clusters

## Included:

- [Docker](https://docs.docker.com/engine/reference/commandline/cli/) `docker`
- [Kubernetes CLI](https://kubernetes.io/docs/tasks/tools/install-kubectl/) `kubectl`
- [Rancher CLI](https://rancher.com/docs/rancher/v2.x/en/cli/) `rancher`

## Usage:

add the following snippet to your `.gitlab-ci.yml` and set `R2_URL`, `R2_TOKEN`, `R2_CONTEXT`, `R2_NAMESPACE` and `R2_WORKLOAD` in GitLabs `Settings` => `CI/CD` => `Variables`:

```
deploy:
  stage: deploy
  image: registry.gitlab.com/schaefer_advise-mc/rancher2-deploy
  script:
    - rancher login $R2_URL --token $R2_TOKEN --context $R2_CONTEXT
    - re-deploy $R2_NAMESPACE $R2_WORKLOAD
```

or

```
deploy:
  stage: deploy
  image: registry.gitlab.com/schaefer_advise-mc/rancher2-deploy
  script:
    - rancher login $R2_URL --token $R2_TOKEN --context $R2_CONTEXT
    - deploy-new $R2_NAMESPACE $R2_WORKLOAD $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
```