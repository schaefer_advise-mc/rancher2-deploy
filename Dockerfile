FROM alpine:latest

# install dependencies
RUN apk update
RUN apk add \
    --no-cache \
    ca-certificates \
    curl \
    git \
    bash \
	docker

RUN apk add \
    --no-cache \
    --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
    rancher-cli \
    kubectl

# set shell to bash
SHELL ["/bin/bash", "-c"]

# add re-deploy script
COPY script/ /usr/local/bin/
RUN chmod +x /usr/local/bin/deploy-new
RUN chmod +x /usr/local/bin/re-deploy

ENTRYPOINT []
